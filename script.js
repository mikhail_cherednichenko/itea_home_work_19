/* Створіть програму секундомір.
- Секундомір матиме 3 кнопки "Старт, Стоп, Скидання"
- При натисканні на кнопку стоп фон секундоміра має бути червоним, старт - зелений, скидання - сірий * Виведення лічильників у форматі ЧЧ:ММ:СС
- Реалізуйте Завдання використовуючи синтаксис ES6 та стрілочні функції */

const hours = document.getElementById("hours");
const minutes = document.getElementById("minutes");
const seconds = document.getElementById("seconds");
const startButton = document.getElementById("start");
const stopButton = document.getElementById("stop");
const resetButton = document.getElementById("reset");
const stopwatchDisplay = document.getElementById("stopwatchDisplay");

let checker = false;

startButton.onclick = () => {
    if (!checker) {
        checker = true;
        stopwatchDisplay.classList.remove('red', 'silver');
        stopwatchDisplay.classList.add('green');
        intervalHandler = setInterval(() => {
            let hh = hours.textContent,
                mm = minutes.textContent,
                ss = seconds.textContent;
            
            ss++;

            if (ss == 60) {
                ss = 0;
                mm++;
            };

            if (mm == 60) {
                mm = 0;
                hh++;
            };

            hours.textContent = ('0' + hh).slice(-2);
            minutes.textContent = ('0' + mm).slice(-2);
            seconds.textContent = ('0' + ss).slice(-2);

            stopwatchDisplay.classList.add('green');
        }, 1000);
    };
};

stopButton.onclick = () => {
    checker = false;
    stopwatchDisplay.classList.remove('green', 'silver');
    stopwatchDisplay.classList.add('red');
    clearInterval(intervalHandler);
};

resetButton.onclick = () => {
    hours.textContent = '00';
    minutes.textContent = '00';
    seconds.textContent = '00';
    stopwatchDisplay.classList.remove('red','green');
    stopwatchDisplay.classList.add('silver');
};


/* Реалізуйте програму перевірки телефону
- Використовуючи JS Створіть поле для введення телефону та кнопку збереження
- Користувач повинен ввести номер телефону у форматі 000-000-00-00
- Після того як користувач натискає кнопку зберегти перевірте правильність введення номера, якщо номер правильний зробіть зелене тло і використовуючи document.location переведіть користувача на сторінку https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg якщо буде помилка, відобразіть її в діві до input. */

const inputTel = document.getElementById("inputTel");
const saveTel = document.getElementById("saveTel");
const uncurrectTel = document.getElementById("uncurrectTel");

saveTel.onclick = () => {
    const pattern = /\d{3}-\d{3}-\d{2}-\d{2}/;
    const tel = inputTel.value;

    if (pattern.test(tel)) {
        inputTel.classList.add('green');
        setTimeout(() => {
            document.location = 'https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg';
        }, 1000);
    } else {
        uncurrectTel.classList.remove('disabled');
    };
}